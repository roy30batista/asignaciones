/*asignacion 12: Elaborar una aplicación de línea de comandos en C que simule un gestor de contactos simple
 de un dispositivo móvil. Esta programa debe permitir agregar nuevos registros, listarlos, modificarlos,
 buscar contactos guardados, eliminar registros telefónicos. Tome en consideración que la información
 personal por almacenar debe ser nombre, teléfono y correo electrónico.*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

typedef struct contacto {
   char nombre[18];
   char numero[22];
   char correo[100];
} contacto;

int main(){

        contacto * persona, *buscar;
        persona = (contacto*) malloc(10 * sizeof(contacto));
        buscar = (contacto*) malloc(10 * sizeof(contacto));

         int opcion = 0, num=1, elim;
         int posicion, x, pos, i;
         char palabra[18];

//inicia menu
while (opcion != 6) {
    printf("\n      \t -------MENU-------");
    printf("\n      \t 1. Añadir Nuevo Contacto");
    printf("\n      \t 2. Lista de Contactos");
    printf("\n      \t 3. Editar Contacto");
    printf("\n      \t 4. Eliminar Contacto");
    printf("\n      \t 5. Buscar");
    printf("\n      \t 6. Salir");
    printf("\n      \t --> Opcion : ");
    scanf("%d", &opcion);
    system("clear");

    switch(opcion){

        case 1: 
            for (x = num-1; x < num; x++) { /* num vale 1 y x vale 0, x es menor que num asique se le suma 1 a x 
                                            y x vale ahora 1, ahora x vale lo mismo que num por lo que se cancela el ciclo */
                printf("\n         Introduzca info del contacto %d \n",num);
                printf("\n       Nombre: ");
                scanf("%s", persona[x].nombre);
                strcpy(buscar[x].nombre, persona[x].nombre);

                printf("       Numero: ");
                scanf("%s", persona[x].numero);
                strcpy(buscar[x].numero, persona[x].numero);

                printf("       Correo: ");
                scanf("%s", persona[x].correo);
                strcpy(buscar[x].numero, persona[x].correo);


            }
                num=num+1; /*como el ciclo ya se cerro, se le incrementa el valor a num ahora num vale 2 y x vale 1 
                            al entrar nuevamente al ciclo se repetira el mismo paso anterior. */
        system("clear");
        printf("\n         Su Contacto ha sido guardado con exito \n");
        break;

        case 2: 
            system("clear");
            printf("\n\n                                  Lista de Contactos ");
            printf("\n\n\t\t     Nombre\t\tTelefono\t\tCorreo ");
            pos=1;
            for (x = 0; x < num-1; x++){ //bucle para recorrer la lista
                printf("\n\t Contacto %i: %s\t\t%s\t\t%s", pos, persona[x].nombre, persona[x].numero, persona[x].correo); //imprime lista de nombres
                pos++;
            }
            printf("\n");

        break;

        case 3:
            printf("\n\n                                  Lista de Contactos \n"); //pregunta que nombre editar
            pos=1; //lo iniciamos en 1 para que no contenga valores de bucles anteriores.
                for (x = 0; x < num-1; x++) {  //bucle para recorrer la lista
                printf("         Contacto %i: %s \n", pos, persona[x].nombre); //imprime la lista
                pos++;
            }
            printf("\n         Seleccione el contacto a editar: ");
            scanf("%d", &posicion);
            posicion=posicion-1; //esto para que al presionar el contacto 1 indique al contacto 0 y asi se vea mas estetico.
            printf("\n         Nombre: ");
                scanf("%s", persona[posicion].nombre);
                strcpy(buscar[posicion].nombre, persona[posicion].nombre);

                printf("         Numero: ");
                scanf("%s", persona[posicion].numero);
                strcpy(buscar[posicion].numero, persona[posicion].numero);

                printf("         Correo: ");
                scanf("%s", persona[posicion].correo);
                strcpy(buscar[posicion].correo, persona[posicion].correo);
        system("clear");
        break;

        case 4:
        printf("\n\n                Lista de contactos \n"); //pregunta que nombre eliminar
            pos=1; //lo iniciamos en 1 para que no contenga valores de bucles anteriores.
                for (x = 0; x < num-1; x++) {  //bucle para recorrer la lista
                printf("\n         Contacto %i: %s ", pos, persona[x].nombre); //imprime la lista
                pos++;  //le incrementamos 1 para que siga el bucle y se vea estetico
            }
            printf("\n\n         Eliminar contacto: ");
            scanf("%d", &posicion);
            posicion=posicion-1; //le eliminamos 1 para que indique al contacto 0 al presionar 1.
            elim=num;   //eliminar adquiere el valor de num para no dañar el ciclo agregar ya que requiere que num mantenga su valor orginal. 
            elim++; //elim se le incrementa para que apunte a un espacio vacio o que no exista.
            persona[posicion]=persona[elim]; //la posicion de la persona actual sera reemplazada por un espacio vacio.
            buscar[posicion]=persona[elim];
        break;

        case 5:
            pos=1;
            printf("        Introduzca el nombre para buscar: \n");
            printf("        Nombre: ");
            scanf("%s",palabra);
            printf("        Contactos Similares\n");
            printf("\n\n\t\t     Nombre\t\tTelefono\t\tCorreo ");
            for (x = 0; x < num-1; x++){    //bucle para recorrer la lista
                for ( i = 0;  i < strlen(buscar[x].nombre) ; i++) { //bucle para recorrer la lista e igualar todos los nombres
                    buscar[x].nombre[i] = toupper(buscar[x].nombre[i]); //toupper pondra en mayuscula todos los nombres para igualarlos
                }
                for ( i = 0;  i < strlen(palabra) ; i++) { //bucle para recorrer la lista
                    palabra[i] = toupper(palabra[i]);   //toupper pondra en mayuscula todos los nombres para igualarlos
                }
            }//se añade este ciclo para convertir todos los nombres a mayuscula
            for (x = 0; x < num-1; x++){ //bucle para recorrer la lista y comparar todos los nombres
                if (strcmp(palabra,buscar[x].nombre)==0) { //una ves igualados con toupper los compara y si son iguales ejecuta instrucciones
                    printf("\n\t       Contacto %i: %s\t\t%s\t\t%s ", pos, persona[x].nombre, persona[x].numero, persona[x].correo); //imprime contactos similares si pasa el strcmp
                    printf("\n");
                    pos++;
                }
            }   

        break;

        case 6:
        break;

        default: printf("Seleccion Invalida\n");
    }

} // termina menu
    free(persona);
    free(buscar);
    return 0;
}