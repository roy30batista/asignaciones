/*asignacion 7: Elaborar una aplicación de línea de comandos en C que dado 10 números,
pueda mostrar cuál es el número mayor y el número de veces que aparece en la serie. */

#include <stdio.h>
#include <string.h>

int main()
{
	int  numero, mayor,menor, i, n=0, acumulador=0;
		
	    printf("\n		Introduzca 10 numeros: \n\n");

		for (i=0; i<10; i++) {

			n++;
			printf("	Numero %d: ",n);
			scanf("%d",&numero);

			if (i == 0){
				mayor = numero;
				menor = numero;
				acumulador=1;

			}
			else {

				if (numero>mayor)	{
					mayor=numero;

					acumulador=1;
					
                }
                else if (numero==mayor)	{
					acumulador++;
					
                }
			}
		}
	    printf("\n	El numero mayor es: %d\n", mayor);
		printf("	Aparecio %d veces\n\n", acumulador);
    return 0;
}
