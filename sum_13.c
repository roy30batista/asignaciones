/*sum_13.c Elaborar una aplicación de línea de comandos en C que simule un sistema de administración bancario. 
Se debe poder crear una cuenta, actualizar información de una cuenta existente, validar los detalles de una cuenta existente,
eliminar una cuenta existente y ver la lista completa de clientes. */

#include <stdio.h>  //
#include <string.h> //null y size of
#include <stdlib.h> //para clear

typedef struct cuenta{
   char     nombre[18];
   float    dinero;
} cuenta;

void leer(); //declaro funcion


void borrartodo(){

    cuenta persona; //estructura y variable de estructura
    FILE *archivo = fopen ("datos", "wb");

        if (archivo != NULL){
            printf("\n      \t Abierto para Crear y Borrar\n");
            fclose(archivo);
        }//fin if

        else {printf("\n      \t Error\n");}   //fin else
}//fin void

void crear(){

    cuenta persona; //estructura y variable de estructura
    char person[18]; //nombre temporal
    FILE *archivo = fopen ("datos", "a+b");

    
        if (archivo != NULL){
            printf("\n      \t          Crear un Usuario\n");
            printf("\n      \t Nombre: ");
            fflush(stdin);
            scanf("%s",person);
            
            fread(&persona, sizeof(cuenta),1,archivo);
            while (!feof(archivo)){
                if (strcmp(person,persona.nombre)==0){
                    system("clear");
                    printf("\n      \t          Crear un Usuario\n");
                    printf("\n      \t El nombre ya existe \n");
                    printf("\n      \t Intente con uno nuevo ");
                    printf("\n      \t Nombre: ");
                    fflush(stdin);
                    scanf("%s",person);
                    system("clear");
                    fseek(archivo,0,SEEK_SET); //va al inicio de la lista
                }//busca nombre igual
            fread(&persona, sizeof(cuenta),1,archivo); //recorre del inicio a fin
            }//recorre lista en busca de nombres

            strcpy(persona.nombre,person);
            persona.dinero=0,00;
            fwrite(&persona, sizeof(cuenta),1,archivo);

            fclose(archivo);
        }//fin if

        else {
            printf("\n      \t Error\n");
        }   //fin else
}//void cargar

void depositar(){

    cuenta persona; //estructura y variable de estructura
    int num=1;
    float dinerotemp;
    char person[18];
    FILE *archivo = fopen ("datos", "r+b");
        if (archivo != NULL){

            printf("\n      \t Entrar a la cuenta de:  ");
            fflush(stdin);
            scanf("%s",person);

            fread(&persona, sizeof(cuenta),1,archivo); //para que el if lo reciba el primer nombre leido
            while (!feof(archivo)){
                
                if (strcmp(person,persona.nombre)==0) {
                    system("clear");
                    printf("\n                  Estado Actualmente  \n");
                    printf("\n      \t Nombre              Dinero");
                    printf("\n      \t %s                $ %.2f \n",persona.nombre,persona.dinero);
                    printf("\n      \t Cuanto dinero desea Depositar?  ");
                    printf("\n      \t Monto: ");
                    fflush(stdin);
                    scanf("%f",&dinerotemp);
                    persona.dinero=persona.dinero+dinerotemp;
                    int posicion=ftell(archivo)-sizeof(cuenta); //adquiere posicion
                    fseek(archivo,posicion,SEEK_SET); //posicion actual
                    fwrite(&persona, sizeof(cuenta),1,archivo); //escribe en posicion actual
                    system("clear");
                    printf("\n                  Estado Actualmente  \n");
                    printf("\n      \t Nombre              Dinero");
                    printf("\n      \t %s                $ %.2f \n",persona.nombre,persona.dinero);
                    printf("\n      \t Dinero Guardado con Exito. \n");
                    printf("\n      \t Se han añadido $ %.2f \n",dinerotemp);

                }

                else{
                }
                fread(&persona, sizeof(cuenta),1,archivo); //que el while recorra el ciclo hastael final
            }


            fclose(archivo);
        }//fin if

        else {
            printf("\n      \t Error El archivo no existe.\n");
        }   //fin else
}//void leer

void pasar(){

    cuenta persona; //estructura y variable de estructura
    int num=1;
    float dinerotemp;
    char person1[18],person2[18];
    FILE *archivo = fopen ("datos", "r+b");
        if (archivo != NULL){

            printf("\n\n      \t          Cual es tu nombre? ");
            printf("\n      \t  Nombre: ");
            scanf("%s",person1);

            fread(&persona, sizeof(cuenta),1,archivo); //leo el primer nombre antes del while
            while (!feof(archivo)){
                
                if (strcmp(person1,persona.nombre)==0) {
                    system("clear");
                    printf("\n                      Estado Actualmente  \n");
                    printf("\n      \t Nombre: %s                  $ %.2f \n",persona.nombre,persona.dinero);
                }

                else{}

            fread(&persona, sizeof(cuenta),1,archivo);
            }

            printf("\n\n      \t          A quien le quieres pasar el dinero? \n ");
            leer();
            printf("\n\n      \t  Nombre: ");
            scanf("%s",person2);
            printf("\n\n      \t  Monto: ");
            scanf("%f",&dinerotemp);       

            fseek(archivo,0,SEEK_SET); //posiciono el puntero binario en cero
                fread(&persona, sizeof(cuenta),1,archivo); //leo el primer nombre antes del while
                while (!feof(archivo)){
                    
                    if (strcmp(person1,persona.nombre)==0) {
                        persona.dinero=persona.dinero-dinerotemp;
                        int posicion=ftell(archivo)-sizeof(cuenta); //adquiere posicion
                        fseek(archivo,posicion,SEEK_SET); //posicion actual
                        fwrite(&persona, sizeof(cuenta),1,archivo); //escribe en posicion actual

                    }
                    else{}

                    if (strcmp(person2,persona.nombre)==0) {
                        persona.dinero=persona.dinero+dinerotemp;
                        int posicion=ftell(archivo)-sizeof(cuenta); //adquiere posicion
                        fseek(archivo,posicion,SEEK_SET); //posicion actual
                        fwrite(&persona, sizeof(cuenta),1,archivo); //escribe en posicion actual

                    }
                    else{}


                fread(&persona, sizeof(cuenta),1,archivo);
                }

                fseek(archivo,0,SEEK_SET); //posiciono el puntero binario en cero
                fread(&persona, sizeof(cuenta),1,archivo); //leo el primer nombre antes del while
                while (!feof(archivo)){
                    
                    if (strcmp(person1,persona.nombre)==0) {
                        system("clear");
                        printf("\n                      Estado Actualmente  \n");
                        printf("\n      \t Nombre: %s                  $ %.2f \n",persona.nombre,persona.dinero);
                        printf("\n      \t          Le enviaste $ %.2f a la cuenta de %s \n",dinerotemp,person2 );
                    }

                    else{}

                    fread(&persona, sizeof(cuenta),1,archivo);
                }

           fclose(archivo); 
        }

        else{}
}

void mostrar(){

    cuenta persona; //estructura y variable de estructura
    char person[18];
    FILE *archivo = fopen ("datos", "rb");
        if (archivo != NULL){

            fseek(archivo,0,SEEK_SET); //posiciono en cero

            printf("\n\n      \t Ver cuenta de:  ");
            scanf("%s",person);

            fread(&persona, sizeof(cuenta),1,archivo); //leo el primer nombre antes del while
            while (!feof(archivo)){
                
                if (strcmp(person,persona.nombre)==0) {
                    system("clear");
                    printf("\n                      Estado Actualmente  \n");
                    printf("\n      \t Nombre: %s                  $ %.2f \n",persona.nombre,persona.dinero);

                }

                else{
                }
                fread(&persona, sizeof(cuenta),1,archivo);
            }


            fclose(archivo);
        }//fin if

        else {
            printf("\n      \t Error El archivo no existe.\n");
        }   //fin else
}//void mostrar

void leer(){

    cuenta persona; //estructura y variable de estructura
    FILE *archivo = fopen ("datos", "rb");
        if (archivo != NULL){
            
            printf("\n                      Cuentas Existentes \n");

            fread(&persona, sizeof(cuenta),1,archivo);
            while(!feof(archivo)){
                printf("\n      \t  %s",persona.nombre);
                fread(&persona, sizeof(cuenta),1,archivo);
            } //fin while para listar
            
            fclose(archivo);
        }//fin if

        else {
            printf("\n      \t Error El archivo no existe.\n");
        }   //fin else
}//void leer

void editar(){

    cuenta persona; 
    char    editar[18];

    FILE *archivo = fopen ("datos", "r+b");


    if (archivo!=NULL){

        printf("\n\n      \t Nombre a Editar: ");
        scanf("%s",editar);

        fread(&persona, sizeof(cuenta),1,archivo);
        while (!feof(archivo)){

            if (strcmp(editar,persona.nombre)==0){
                printf("\n      \t Ingrese el nuevo nombre: ");
                scanf("%s",persona.nombre);
                int posicion=ftell(archivo)-sizeof(cuenta);
                fseek(archivo,posicion,SEEK_SET);
                fwrite(&persona, sizeof(cuenta),1,archivo);
                printf("\n      \t Se ha Modificado su cuenta con exito");
                break;
            }//busca nombre igual
                

        }//recorre lista en busca de nombres

        fclose(archivo);
    } //fin if

    else {
        printf("\n      \t Error El archivo no existe.\n");
    } //fin else
} //void editar


void eliminar(){

    cuenta persona; 
    char    borrar[18];
    int     elimina=0, num=1;
    FILE *archivo = fopen ("datos", "rb");
    FILE *eliminar = fopen ("temporal", "wb");

    if (archivo != NULL || eliminar!=NULL){

        printf("\n\n      \t Nombre a eliminar: ");
        fflush(stdin);
        scanf("%s",borrar);
            fread(&persona, sizeof(cuenta),1,archivo); //leo el primer nombre antes del while
            while (!feof(archivo)){
                
                if (strcmp(borrar,persona.nombre)==0) {
                    printf("\n      \t Se ha borrado con exito. ");
                }

                else{
                    num++;
                    fwrite(&persona, sizeof(cuenta),1,eliminar);
                }
                fread(&persona, sizeof(cuenta),1,archivo);
            }

            if (num==1)
            {
                printf("\n\n      \t Introdujo un Nombre incorrecto o no existe.");
            }


            fclose(archivo);
            fclose(eliminar);

            remove("datos");
            rename("temporal","datos");
    }

    else{
        printf("\n      \t Error El archivo no existe.\n");
    }

}//void eliminar



int main(){

    int opcion;
    while (opcion !=9) /*menu*/{

    printf("\n      \t -------MENU-------");
    printf("\n      \t 1. Eliminar Todo");
    printf("\n      \t 2. Crear Cuenta");
    printf("\n      \t 3. Cuentas Existentes");
    printf("\n      \t 4. Editar Cuenta");
    printf("\n      \t 5. Eliminar Cuenta");
    printf("\n      \t 6. Mostrar");
    printf("\n      \t 7. Depositar dinero");
    printf("\n      \t 8. Pasar Dinero");
    printf("\n      \t 9. Salir");
    printf("\n      \t --> Opcion : ");
    scanf("%d", &opcion); //pide un numero por telcado


        switch (opcion) /* lee el numero que elegimos */{

            case 1: system("clear");
                    borrartodo();
                    printf("\n");
            break;

            case 2: system("clear");
                    crear();
            break;

            case 3: system("clear");
                    leer();
                    printf("\n");
            break;

            case 4: system("clear");
                    leer();
                    editar();
                    printf("\n");
            break;

            case 5: system("clear");
                    leer();
                    eliminar();
                    printf("\n");
            break;

            case 6: system("clear");
                    leer();
                    mostrar();
            break;

            case 7: system("clear");
                    leer();
                    depositar();
            break;

            case 8: system("clear");
                    leer();
                    pasar();

            break;

            case 9:
            break;

        }
    } /* termina menu */

    return 0;
}