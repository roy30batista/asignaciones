/*asignacion 6: Elaborar una aplicación de línea de comandos en C que lea los nombres de 5 personas 
por teclado y que determine cuál de esas tiene el nombre más corto. */
#include <stdio.h>
#include <string.h>

int main()
{
	char nombre[100], palabra[100];
	int  longitud=0, mayor,menor, i, n=0;
		
	    printf("\n		Introduzca 5 nombres: \n");

		for (i=0; i<5; i++) {

			n++;
			printf("\n	Nombre %d: ",n);
			scanf("%s",nombre);
			// gets(nombre);
			longitud = strlen(nombre);
			printf ("	Tiene %d caracteres\n", longitud);
				
			if (i == 0){
				mayor = longitud;
				menor = longitud;
				memcpy(palabra, nombre, strlen(nombre)+1);
			}
			else {

				if (longitud<=menor)	{
					menor=longitud;	
					/*printf("Es la menor Actualmente con %d caracteres \n",menor);*/
					memcpy(palabra, nombre, strlen(nombre)+1);
                }
			}
		}
	    printf("\n	Nombre mas corto: %s \n	Con %d caracteres\n\n", palabra, menor);

    return 0;
}
