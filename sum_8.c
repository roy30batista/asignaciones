/*asignacion 8: Elaborar una aplicación de línea de comandos en C que luego de introducir un 
valor numérico entero positivo pueda mostrar su tabla de multiplicar. */

#include <stdio.h>

int main()
{
	int  i, num,acum=0,result;
	
	 printf("\n		Introduzca un numero: "); scanf("%d", &num);
	printf("\n	Su tabla de multiplicar es: \n");
		for (i=0; i<12; i++) {
			acum=acum+1;
			result=num*acum;
			printf("	%d x %d = %d	 \n",num,acum, result);
		}

printf("\n");
    return 0;
}
