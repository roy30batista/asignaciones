/*asignacion 4: Actividad: Elaborar una aplicación de línea de comandos en C que calcule el promedio
de las ventas diarias (lunes a viernes) de los tres vendedores que tiene la empresa 
NASA (Nutrición Animal S.A.).
Nota: El código fuente debe ser subido a un repositorio en GitLab/Github.
En este espacio se sube el URL hacia el repositorio*/


#include <stdio.h>

int main() {

	float ventas[5], prom, prom_vend;
	int dia = 0;

	printf("\n        NASA (Nutrición Animal S.A.).      \n\n");

	    for (int i=0;i<5;i++){
		
	    	dia= dia + 1;
		
		    printf("Introduzca la venta del dia %d: ", dia);
		    scanf("%f", &ventas[i]);
        }

        prom = (ventas[0] + ventas[1] + ventas[2] + ventas[3] + ventas[4] ) / 5;
	    prom_vend = prom / 3;

	printf ("\nEl promedio de ventas diarias es de: %.2f \n", prom);
	printf ("El promedio de venta entre los 3 vendedores seria de: %.2f \n\n", prom_vend);
	
	return 0;
}
