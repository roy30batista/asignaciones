/*Asignacion 3: Elaborar una aplicación de línea de comandos en C que calcule la expresión
de segundo grado ax^2 + bx + c = 0 */
#include <stdio.h>
#include <math.h>

int main() {
	
	float a, b, c, d, p, n;

	printf("\nEcuaciones de Segundo Grado \n\n");
	printf("ax^2 + bx + c = 0\n\n");

	    printf("Introduce el valor de a: ");
	    scanf("%f", &a);
		
	    printf("Introduce el valor de b: ");
	    scanf("%f", &b);

	    printf("Introduce el valor de c: ");
	    scanf("%f", &c);
	
	        d= b*b - (4*a*c);

	        p= (-b+sqrt(d))/(2*a);
	        n= (-b-sqrt(d))/(2*a);
		
		printf("\n El resultado es: \n");

	    if (d<0)
	    {
	    	printf("\nSolucion Imaginaria\n");
	    	d = fabs(d);
	    	p= (-b+sqrt(d))/(2*a);
	        n= (-b-sqrt(d))/(2*a);
	    }

	printf("Solucion Positiva: %.2f\n", p);
	printf("Solucion Negativa: %.2f\n", n);

	return 0;
}
