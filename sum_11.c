/*asignacion 11: Elaborar una aplicación de línea de comandos en C diseñada para determinar el horóscopo,
 y predecir el futuro de un usuario basado en la fecha de nacimiento, nombre y sexo. 
 Esta aplicación deberá determinar tu signo zodiacal, y predecir cómo será el futuro, 
 cómo estará de salud, qué pasará con las relaciones, cuáles son tu número y color de la suerte. 
 Estas predicciones y cálculos se basarán en la configuración astrológica que ocurrió al momento de la fecha de nacimiento provista por el usuario. */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main()
{
	
	int dia, mes, ano,sexo_n,edad; 
	char nombre[12], sexo[30], mes_n[12],mof;
		
		printf("\n				 Introduce tu nombre: ");
			if (fgets(nombre, sizeof nombre, stdin) != NULL) {
	    			// Remove potential \n
	    			nombre[strcspn(nombre, "\n")] = 0;
			}

		printf("			 Introduce tu sexo (F) o (M): ");
			scanf("%s",&mof);

		volver:
			
		printf("      Introduce tu fecha de nacimiento (dia/mes/año): ");
		scanf("%d %d %d",&dia,&mes,&ano);
		
		system("clear");
		
		
		
		switch(mof){ //Decide el sexo
			case 'm':
			case 'M': strcpy(sexo, "Masculino");
				break;
			case 'f':
			case 'F': strcpy(sexo, "Femenino");
				break;
			default: strcpy(sexo, "No Definido");
		}		
				

		switch(mes){	//pasa numero del mes a letra
			case 1: strcpy(mes_n, "Enero");
				break;
			case 2: strcpy(mes_n, "Febrero");
				break;
			case 3: strcpy(mes_n, "Marzo");
				break;
			case 4: strcpy(mes_n, "Abril");
				break;	
			case 5: strcpy(mes_n, "Mayo");
				break;
			case 6: strcpy(mes_n, "Junio");
				break;
			case 7: strcpy(mes_n, "Julio");
				break;	
			case 8: strcpy(mes_n, "Agosto");
				break;	
			case 9: strcpy(mes_n, "Septiembre");
				break;	
			case 10: strcpy(mes_n, "Octubre");
				break;	
			case 11: strcpy(mes_n, "Noviembre");
				break;	
			case 12: strcpy(mes_n, "Diciembre");
				break;		
			default: strcpy(mes_n, "No Existe");
			
			}
			
				while (mes>12){ //si el mes es mayor que 12 vuelve a pedir fecha
					printf("\n	El mes no existe o es incorrecto\n\n");
					goto volver;
				}
				if (dia>31){ 
				printf("\n		El dia no existe, intentelo nuevamente \n\n");
					goto volver;
				}
			edad=2021-ano;
			printf("\n   Tu fecha de nacimiento es:	%d/%s/%d",dia,mes_n,ano);
			printf("\n		Tu nombre es:	%s ",nombre);
			printf("\n		      Tienes:	%d años ",edad);
			printf("\n			Sexo:	%s ",sexo);
		
			
			{	//21 Ene - 19 Feb acuario
				if (mes==1 && dia>=21 && dia<=31){ //del 21 al 31 de enero es Acuario 
				
					printf("\n		       Signo:	Acuario");
					printf("\n	Números de la suerte: 	58 y 73");
					printf("\n	  Color de la suerte:	Turquesa");

					printf("\n\n 	Su Futuro:");
					printf("\n	Tu signo se muestra transparente como el agua y por ende procura ser así ");
					printf("\n	en todas tus relaciones. No dejes que te ahoguen los problemas. ");

					printf("\n\n 	Su Salud:");
					printf("\n	Eres de naturaleza fuerte ante el malestar. Sigue cuidándote como lo haces, ");
					printf("\n	sobre todo con lo que te llevas a la boca ");
					
					printf("\n\n 	Las Relaciones:");
					printf("\n	Déjate tocar por la influencia positiva del amor. Nunca permitas que una ");
					printf("\n	ilusión se desvanezca por tu mal consentimiento. Ábrete a las propuestas. \n ");	
				}
			
					if  (mes==2 && dia>=1 && dia<=19){	//del 1 al 19 de febrero es Acuario 

						printf("\n		       Signo:	Acuario");
						printf("\n	Números de la suerte: 	58 y 73");
						printf("\n	  Color de la suerte:	Turquesa");

						printf("\n\n 	Su Futuro:");
						printf("\n	Tu signo se muestra transparente como el agua y por ende procura ser así ");
						printf("\n	en todas tus relaciones. No dejes que te ahoguen los problemas. ");

						printf("\n\n 	Su Salud:");
						printf("\n	Eres de naturaleza fuerte ante el malestar. Sigue cuidándote como lo haces, ");
						printf("\n	sobre todo con lo que te llevas a la boca ");
						
						printf("\n\n 	Las Relaciones:");
						printf("\n	Déjate tocar por la influencia positiva del amor. Nunca permitas que una ");
						printf("\n	ilusión se desvanezca por tu mal consentimiento. Ábrete a las propuestas. \n ");	
					}
			}
			
			{	//20 feb - 20 mar piscis
				if  (mes==2 && dia>=20 && dia<=28){ //del 20 al 28 de febrero es Piscis 
				
					printf("\n		       Signo:	Piscis");
					printf("\n	Números de la suerte: 	76 y 84");
					printf("\n	  Color de la suerte:	Morado");

					printf("\n\n 	Su Futuro:");
					printf("\n	Las corrientes de los astros pudieran arrastrarte mar adentro, lejos de");
					printf("\n	los problemas. Déjate llevar. ");

					printf("\n\n 	Su Salud:");
					printf("\n	Prepara tu propio desodorante natural ya que los de marcas comerciales te");
					printf("\n	causan mal olor. En YouTube existen cientos de tutoriales y te recomendamos ");
					printf("\n 	alguno que utilice aceite de coco o vinagre.");
					
					printf("\n\n 	Las Relaciones:");
					printf("\n	Presta atención  a tu alrededor. Allí podría estar la posibilidad de ");
					printf("\n	pescar un buen partido. Mantente alerta. \n ");
				}
				
					if  (mes==3 && dia>=1 && dia<=20){  // del 1 al 20 marzo es piscis

						printf("\n		       Signo:	Piscis");
						printf("\n	Números de la suerte: 	76 y 84");
						printf("\n	  Color de la suerte:	Morado");

						printf("\n\n 	Su Futuro:");
						printf("\n	Las corrientes de los astros pudieran arrastrarte mar adentro, lejos de");
						printf("\n	los problemas. Déjate llevar. ");

						printf("\n\n 	Su Salud:");
						printf("\n	Prepara tu propio desodorante natural ya que los de marcas comerciales te");
						printf("\n	causan mal olor. En YouTube existen cientos de tutoriales y te recomendamos ");
						printf("\n 	alguno que utilice aceite de coco o vinagre.");
						
						printf("\n\n 	Las Relaciones:");
						printf("\n	Presta atención  a tu alrededor. Allí podría estar la posibilidad de ");
						printf("\n	pescar un buen partido. Mantente alerta. \n ");
					}
			}
		
			{	//21 mar - 20 abr aries
				if  (mes==3 && dia>=21 && dia<=31){		//del 20 al 31 de marzo es Aries 
					
					printf("\n		       Signo:	Aries		");
					printf("\n	Números de la suerte: 	56 y 69");
					printf("\n	  Color de la suerte:	Rojo");
							
					printf("\n\n 	Su Futuro:");
					printf("\n	Esta semana encontrarás la luz que necesitas para afrontar ese proyecto");
					printf("\n	que ves tan lejano.\n");
							
					printf("\n\n 	Su Salud:");
					printf("\n	En el amor tendrás una cita a ciegas con alguien que te derretirá.\n");
						
					printf("\n\n 	Las Relaciones:");
					printf("\n	Cepillate los dientes tres veces al día para evitar el mal aliento.\n");
				}
				
					if  (mes==4 && dia>=1 && dia<=20){	//del 1 al 20 de abril es Aries 
						
						printf("\n		       Signo:	Aries		");
						printf("\n	Números de la suerte: 	56 y 69");
						printf("\n	  Color de la suerte:	Rojo");
								
						printf("\n\n 	Su Futuro:");
						printf("\n	Esta semana encontrarás la luz que necesitas para afrontar ese proyecto");
						printf("\n	que ves tan lejano.\n");
								
						printf("\n\n 	Su Salud:");
						printf("\n	En el amor tendrás una cita a ciegas con alguien que te derretirá.\n");
						
						printf("\n\n 	Las Relaciones:");
						printf("\n	Cepillate los dientes tres veces al día para evitar el mal aliento.\n");
					}
			}
		
			{	//21 abr - 21 may Tauro
				if  (mes==4 && dia>=21 && dia<=30){		//del 21 al 30 de Abril es Tauro 
					
					printf("\n		       Signo:	Tauro");
					printf("\n	Números de la suerte: 	12 y 77");
					printf("\n	  Color de la suerte:	Verde");
							
					printf("\n\n 	Su Futuro:");
					printf("\n	Necesitas relajarte con tus amigos. El estrés del trabajo te tiene mal,");
					printf("\n	no todo es esfuerzo ¡Cuídate!");
							
					printf("\n\n 	Su Salud:");
					printf("\n	Para acabar con los hongos de una vez por todas empieza a utilizar talco en");
					printf("\n	tu calzado y solucionarás el problema.");
					
					printf("\n\n 	Las Relaciones:");
					printf("\n	Para el amor, los astros dicen que lo que necesitas para encender la chispa");
					printf("\n	en tu relación es preparar una cena romántica que incluya velas y vino.\n");
				}
				
					if  (mes==5 && dia>=1 && dia<=21){	//del 1 al 21 de Mayo es Tauro 
						
						printf("\n		       Signo:	Tauro");
						printf("\n	Números de la suerte: 	12 y 77");
						printf("\n	  Color de la suerte:	Verde");
								
						printf("\n\n 	Su Futuro:");
						printf("\n	Necesitas relajarte con tus amigos. El estrés del trabajo te tiene mal,");
						printf("\n	no todo es esfuerzo ¡Cuídate!");
								
						printf("\n\n 	Su Salud:");
						printf("\n	Para acabar con los hongos de una vez por todas empieza a utilizar talco en");
						printf("\n	tu calzado y solucionarás el problema.");
						
						printf("\n\n 	Las Relaciones:");
						printf("\n	Para el amor, los astros dicen que lo que necesitas para encender la chispa");
						printf("\n	en tu relación es preparar una cena romántica que incluya velas y vino.\n");
					}
			}	
		
			{	//22 may - 21 jun Geminis
				if  (mes==5 && dia>=22 && dia<=31){		//del 22 al 31 de Mayo es Geminis 
					
					printf("\n		       Signo:	Géminis		");
					printf("\n	Números de la suerte: 	07 y 74");
					printf("\n	  Color de la suerte:	Mostaza");
							
					printf("\n\n 	Su Futuro:");
					printf("\n	No te preocupes tanto por lo que dicen de ti las redes sociales. Recuerda");
					printf("\n	que existe una vida real y que no necesitas tanta falsedad en tu vida");
							
					printf("\n\n 	Su Salud:");
					printf("\n	Esa gastritis necesita de un especialista, pero sobre todo deja de consumir");
					printf("\n	comida picante y licor.");
				
					printf("\n\n 	Las Relaciones:");
					printf("\n	Al fin lograrás ser feliz al lado de alguien muy cercano a ti a quien le");
					printf("\n	gustas. Solo déjate llevar y deja de ser tan meticuloso.\n");
				}

					if  (mes==6 && dia>=1 && dia<=21){	//del 1 al 21 de Junio es Geminis 
						
						printf("\n		       Signo:	Géminis		");
						printf("\n	Números de la suerte: 	07 y 74");
						printf("\n	  Color de la suerte:	Mostaza");
								
						printf("\n\n 	Su Futuro:");
						printf("\n	No te preocupes tanto por lo que dicen de ti las redes sociales. Recuerda");
						printf("\n	que existe una vida real y que no necesitas tanta falsedad en tu vida");
								
						printf("\n\n 	Su Salud:");
						printf("\n	Esa gastritis necesita de un especialista, pero sobre todo deja de consumir");
						printf("\n	comida picante y licor.");
					
						printf("\n\n 	Las Relaciones:");
						printf("\n	Al fin lograrás ser feliz al lado de alguien muy cercano a ti a quien le");
						printf("\n	gustas. Solo déjate llevar y deja de ser tan meticuloso.\n");
					}
			}
		
			{	//22 jun - 22 jul Cancer
				if  (mes==6 && dia>=22 && dia<=30){		//del 22 al 31 de Junio es Cancer 
					
					printf("\n		       Signo:	Cáncer		");
					printf("\n	Números de la suerte: 	89 y 24");
					printf("\n	  Color de la suerte:	Gris");
							
					printf("\n\n 	Su Futuro:");
					printf("\n	Aléjate de los chismes y de los problemas. Si quieres tener buenas relaciones");
					printf("\n	con tus compañeros deja el bochinche y dedicate solo a sacar lo mejor de ti.");
							
					printf("\n\n 	Su Salud:");
					printf("\n	Para la buena digestión y evitar esas flatulencias a media tarde, toma té");
					printf("\n	de menta luego de cada comida y de seguro te empezarás a sentir mejor.");
				
					printf("\n\n 	Las Relaciones:");
					printf("\n	Tu expareja te intenta seducir lo que es muy peligroso. Aléjate inmediatamente");
					printf("\n	o solo conseguirás problemas.\n");
				}
				
					if  (mes==7 && dia>=1 && dia<=22){	//del 1 al 22 de Julio es Cancer 
						
						printf("\n		       Signo:	Cáncer		");
						printf("\n	Números de la suerte: 	89 y 24");
						printf("\n	  Color de la suerte:	Gris");
								
						printf("\n\n 	Su Futuro:");
						printf("\n	Aléjate de los chismes y de los problemas. Si quieres tener buenas relaciones");
						printf("\n	con tus compañeros deja el bochinche y dedicate solo a sacar lo mejor de ti.");
								
						printf("\n\n 	Su Salud:");
						printf("\n	Para la buena digestión y evitar esas flatulencias a media tarde, toma té");
						printf("\n	de menta luego de cada comida y de seguro te empezarás a sentir mejor.");
					
						printf("\n\n 	Las Relaciones:");
						printf("\n	Tu expareja te intenta seducir lo que es muy peligroso. Aléjate inmediatamente");
						printf("\n	o solo conseguirás problemas.\n");
					}
			}
		
			{	//23 Jul - 23 Ago Leo
				if  (mes==7 && dia>=23 && dia<=31){		//del 23 al 31 de Julio es Leo
					 
					printf("\n		       Signo:	Leo		");
					printf("\n	Números de la suerte: 	62 y 94");
					printf("\n	  Color de la suerte:	Naranja");
							
					printf("\n\n 	Su Futuro:");
					printf("\n	Tu signo está marcado por la fuerza y el dominio salvaje así que demuestra");
					printf("\n	quién tiene el mando en cada aspecto de tu vida. Toma el control.");
							
					printf("\n\n 	Su Salud:");
					printf("\n	Deja de beber todos los días. Tu hígado ya no aguanta. Dale un respiro.");
				
					printf("\n\n 	Las Relaciones:");
					printf("\n	Tu relación en pareja se encuentra atravesando un momento difícil, la solución");
					printf("\n	es la confianza mutua que solo se logra con la comunicación sincera.\n");
				}
				
					if  (mes==8 && dia>=1 && dia<=23){	//del 1 al 23 de Agosto es Leo 
						
						printf("\n		       Signo:	Leo		");
						printf("\n	Números de la suerte: 	62 y 94");
						printf("\n	  Color de la suerte:	Naranja");
								
						printf("\n\n 	Su Futuro:");
						printf("\n	Tu signo está marcado por la fuerza y el dominio salvaje así que demuestra");
						printf("\n	quién tiene el mando en cada aspecto de tu vida. Toma el control.");
								
						printf("\n\n 	Su Salud:");
						printf("\n	Deja de beber todos los días. Tu hígado ya no aguanta. Dale un respiro.");
					
						printf("\n\n 	Las Relaciones:");
						printf("\n	Tu relación en pareja se encuentra atravesando un momento difícil, la solución");
						printf("\n	es la confianza mutua que solo se logra con la comunicación sincera.\n");
					}
			}
			
			{	//24 Ago - 23 Sep Virgo
				if  (mes==8 && dia>=24 && dia<=31){		//del 24 al 31 de Agosto es Virgo 
					
					printf("\n		       Signo:	Virgo		");
					printf("\n	Números de la suerte: 	66 y 69");
					printf("\n	  Color de la suerte:	Chocolate");
								
					printf("\n\n 	Su Futuro:");
					printf("\n	Deja de ser tan infiel. Comprende que el amor es solo de dos y cuando hay tres");
					printf("\n	personas se forma una guerra innecesaria donde sólo el perdedor eres tú.");
								
					printf("\n\n 	Su Salud:");
					printf("\n	Deja de comer tanto chicharrón y colesterol, a tu edad los vegetales te");
					printf("\n	harán lucir radiante.");
					
					printf("\n\n 	Las Relaciones:");
					printf("\n	Tu relación en pareja se encuentra atravesando un momento difícil, la solución");
					printf("\n	es la confianza mutua que solo se logra con la comunicación sincera.\n");
				}
				
					if  (mes==9 && dia>=1 && dia<=23){	//del 1 al 23 de Septiembre es Virgo 
						
						printf("\n		       Signo:	Virgo		");
						printf("\n	Números de la suerte: 	66 y 69");
						printf("\n	  Color de la suerte:	Chocolate");
									
						printf("\n\n 	Su Futuro:");
						printf("\n	Deja de ser tan infiel. Comprende que el amor es solo de dos y cuando hay tres");
						printf("\n	personas se forma una guerra innecesaria donde sólo el perdedor eres tú.");
									
						printf("\n\n 	Su Salud:");
						printf("\n	Deja de comer tanto chicharrón y colesterol, a tu edad los vegetales te");
						printf("\n	harán lucir radiante.");
						
						printf("\n\n 	Las Relaciones:");
						printf("\n	Tu relación en pareja se encuentra atravesando un momento difícil, la solución");
						printf("\n	es la confianza mutua que solo se logra con la comunicación sincera.\n");
					}
			}

			{	//24 Sep - 23 Oct Libra
				if  (mes==9 && dia>=24 && dia<=30){		//del 24 al 30 de Septiembre es Libra 
					
					printf("\n		       Signo:	Libra		");
					printf("\n	Números de la suerte: 	70 y 87");
					printf("\n	  Color de la suerte:	Azul");
								
					printf("\n\n 	Su Futuro:");
					printf("\n	Rodéate de personas positivas y atraerás la misma vibración a tu vida.");
					printf("\n	Recuerda que nuestras acciones a los demás se regresan a nosotros mismos");
					printf("\n	por lo que debes tratar a los demás como te gustaría que te trataran.");			
								
					printf("\n\n 	Su Salud:");
					printf("\n	Lévate el cabello con frecuencia. Recuerda que la apariencia física no es");
					printf("\n	tan importante, pero sí el aseo personal. échale ganas.");
					
					printf("\n\n 	Las Relaciones:");
					printf("\n	Deja de rogarle, ten dignidad. Si las cosas no se dan acéptalo sin más y");
					printf("\n	recuerda que tu autoestima siempre debe ir en lo alto.\n");
				}
				
					if  (mes==10 && dia>=1 && dia<=23){	//del 1 al 23 de Octubre es Libra 
						
						printf("\n		       Signo:	Libra		");
						printf("\n	Números de la suerte: 	70 y 87");
						printf("\n	  Color de la suerte:	Azul");
									
						printf("\n\n 	Su Futuro:");
						printf("\n	Rodéate de personas positivas y atraerás la misma vibración a tu vida.");
						printf("\n	Recuerda que nuestras acciones a los demás se regresan a nosotros mismos");
						printf("\n	por lo que debes tratar a los demás como te gustaría que te trataran.");			
									
						printf("\n\n 	Su Salud:");
						printf("\n	Lévate el cabello con frecuencia. Recuerda que la apariencia física no es");
						printf("\n	tan importante, pero sí el aseo personal. échale ganas.");
						
						printf("\n\n 	Las Relaciones:");
						printf("\n	Deja de rogarle, ten dignidad. Si las cosas no se dan acéptalo sin más y");
						printf("\n	recuerda que tu autoestima siempre debe ir en lo alto.\n");
					}
			}

			{	//24 Oct - 22 Nov escorpio
				if  (mes==10 && dia>=24 && dia<=31){		//del 24 al 31 de Octubre es escorpio 
					
					printf("\n		       Signo:	Ecorpio		");
					printf("\n	Números de la suerte: 	09 y 25");			
					printf("\n	  Color de la suerte:	Rojo");
								
					printf("\n\n 	Su Futuro:");
					printf("\n	Las estrellas se mueven en grupo y tu deberías imitarlas. Nos referimos a que");
					printf("\n	para todo en la vida, andar de solitarios y creernos los héroes no nos servirá");
					printf("\n	de mucho. Por más que te cueste creerlo, siempre necesitarás de los demás sin");
					printf("\n	importar que pienses que X o Y persona jamás te ayudaría en nada.");				
								
					printf("\n\n 	Su Salud:");
					printf("\n	Acude a un otorrino y deja de abusar con la limpieza con palillos de oídos.");
					printf("\n	Es más peligroso de lo que piensas.");
					
					printf("\n\n 	Las Relaciones:");
					printf("\n	Sorprende a tu pareja. Compra ropa interior sexy o crema batida. La cosa es");
					printf("\n	ser creativos señores. No dejes que la pasión muera.\n");
				}
				
					if  (mes==11 && dia>=1 && dia<=22){	//del 1 al 22 de Noviembre es scorpio 
						
						printf("\n		       Signo:	Ecorpio		");
						printf("\n	Números de la suerte: 	09 y 25");			
						printf("\n	  Color de la suerte:	Rojo");
									
						printf("\n\n 	Su Futuro:");
						printf("\n	Las estrellas se mueven en grupo y tu deberías imitarlas. Nos referimos a que");
						printf("\n	para todo en la vida, andar de solitarios y creernos los héroes no nos servirá");
						printf("\n	de mucho. Por más que te cueste creerlo, siempre necesitarás de los demás sin");
						printf("\n	importar que pienses que X o Y persona jamás te ayudaría en nada.");				
									
						printf("\n\n 	Su Salud:");
						printf("\n	Acude a un otorrino y deja de abusar con la limpieza con palillos de oídos.");
						printf("\n	Es más peligroso de lo que piensas.");
						
						printf("\n\n 	Las Relaciones:");
						printf("\n	Sorprende a tu pareja. Compra ropa interior sexy o crema batida. La cosa es");
						printf("\n	ser creativos señores. No dejes que la pasión muera.\n");
					}
			}
			
			{	//23 nov - 21 dic sagitario
				if  (mes==11 && dia>=23 && dia<=30){	//del 23 al 30 de noviembre es sagitario 
					
					printf("\n		       Signo:	Sagitario		");
					printf("\n	Números de la suerte: 	70 y 46");			
					printf("\n	  Color de la suerte:	Rojo");
								
					printf("\n\n 	Su Futuro:");
					printf("\n	No insinúes que todo el mundo debe pensar como tú y no trates de que las");
					printf("\n	personas tengan las mismas ideologías. Te evitarás muchas riñas y malos");
					printf("\n	entendidos. Trata de ser tolerante y respetar las opiniones ajenas.");			
								
					printf("\n\n 	Su Salud:");
					printf("\n	el uso del condón es de suma importancia si vas por la vida teniendo encuentros");
					printf("\n	alocados. Recuerda que existen enfermedades mortales. Cuidado.");
					
					printf("\n\n 	Las Relaciones:");
					printf("\n	Hacerte el resistente al amor no te servirá de nada por que tarde o temprano");
					printf("\n	encontrarás a una persona de la cual te puedes volver a enamorar.\n");
				}
					if  (mes==12 && dia>=1 && dia<=21){	//del 1 al 21 de diciembre es sagitario 
						
						printf("\n		       Signo:	Sagitario		");
						printf("\n	Números de la suerte: 	70 y 46");			
						printf("\n	  Color de la suerte:	Rojo");
									
						printf("\n\n 	Su Futuro:");
						printf("\n	No insinúes que todo el mundo debe pensar como tú y no trates de que las");
						printf("\n	personas tengan las mismas ideologías. Te evitarás muchas riñas y malos");
						printf("\n	entendidos. Trata de ser tolerante y respetar las opiniones ajenas.");			
									
						printf("\n\n 	Su Salud:");
						printf("\n	el uso del condón es de suma importancia si vas por la vida teniendo encuentros");
						printf("\n	alocados. Recuerda que existen enfermedades mortales. Cuidado.");
						
						printf("\n\n 	Las Relaciones:");
						printf("\n	Hacerte el resistente al amor no te servirá de nada por que tarde o temprano");
						printf("\n	encontrarás a una persona de la cual te puedes volver a enamorar.\n");
					}
			}
			
			{	//22 dic - 20 ene capricornio
				if  (mes==12 && dia>=22 && dia<=31){	//del 22 al 31 de diciembre es capricornio 
				
					printf("\n		       Signo:	Capricornio	");
						printf("\n	Números de la suerte: 	40 y 87");			
						printf("\n	  Color de la suerte:	Negro");
									
						printf("\n\n 	Su Futuro:");
						printf("\n	No te dejes llevar por el capricho y cuida tu dinero. Evita comprar todo");
						printf("\n	lo que ves y sé firme al momento de adquirir. Esté es la única manera");
						printf("\n	con la cuál podrás ahorrar.");			
									
						printf("\n\n 	Su Salud:");
						printf("\n	El ombligo no parece importante pero hay que limpiarlo y mantenerlo fresco");
						printf("\n	para evitar malos olores. Ya lo sabes!");
						
						printf("\n\n 	Las Relaciones:");
						printf("\n	El amor no cae del cielo, tú tienes que empeñarte en su cultivo y por");
						printf("\n	ello procura crear las condiciones positivas.\n");
				}
					if  (mes==1 && dia>=1 && dia<=20){	//del 1 al 20 de enero es capricornio 
					
						printf("\n		       Signo:	Capricornio	");
						printf("\n	Números de la suerte: 	40 y 87");			
						printf("\n	  Color de la suerte:	Negro");
									
						printf("\n\n 	Su Futuro:");
						printf("\n	No te dejes llevar por el capricho y cuida tu dinero. Evita comprar todo");
						printf("\n	lo que ves y sé firme al momento de adquirir. Esté es la única manera");
						printf("\n	con la cuál podrás ahorrar.");			
									
						printf("\n\n 	Su Salud:");
						printf("\n	El ombligo no parece importante pero hay que limpiarlo y mantenerlo fresco");
						printf("\n	para evitar malos olores. Ya lo sabes!");
						
						printf("\n\n 	Las Relaciones:");
						printf("\n	El amor no cae del cielo, tú tienes que empeñarte en su cultivo y por");
						printf("\n	ello procura crear las condiciones positivas.\n");
					}
			}
		printf("\n");	
    return 0;
}
