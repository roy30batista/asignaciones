/*asignacion 10: Elaborar una aplicación de línea de comandos en C que permita introducir un número por teclado. 
Debe contemplar estas referencias: si el número es 15 debe mostrar su nombre en letras,
y si es diferente a 15 entonces debe mostrar su potencia al cubo (^3). */

//para que ejecute usar -lm a la hora de compilar

#include <stdio.h>
#include <math.h>

int main()
{
	
	int  num;
	
		printf("\n	Introduce un Numero: ");
		scanf("%d",&num);
		
		if (num==15){
			printf("	Numero: Quince\n");
		}
		else{
			num=pow(num,3);
			printf("	La potencia del numero al cubo es: %d\n",num);
		}
		
    return 0;
}
