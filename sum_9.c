/*asignacion 9: Elaborar una aplicación de línea de comandos en C que pueda calcular y presentar cuántas cifras
 tiene un determinado valor numérico introducido por teclado. */

#include <stdio.h>
#include <string.h>

int main()
{

	int  cifra, num=0;

	printf("\n	Introduce un valor numerico: ");
	scanf("%i",&cifra);

		while (cifra > 0) {
			cifra=cifra/10;
			num++;
		}
	printf("\n	El valor numerico tiene %i cifras \n\n",num);

    return 0;
}
