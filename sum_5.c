/* Asignacion 5: Elaborar una aplicación de línea de comandos en C que permita al usuario adivinar una 
palabra secreta. El usuario tiene un máximo de 3 intentos para lograrlo. Los intentos ejecutados por
el usuario deben aparecer en pantalla.
Nota: El código fuente debe ser subido a un repositorio en GitLab/Github. En este espacio se sube el 
URL hacia el repositorio */

#include <stdio.h>
#include <string.h>

int main () {

	char clave[100];	
	int i=0;
    int intento = 1;

    printf("\n                  Adivine la palabra secreta");
    printf("\n                  **************************\n");
	printf("\n(Pista: Es un animal que fue considerado como un Dios por los egipcios.) \n");

		do {

	        printf("\nIntroduzca la palabra secreta:	");
		    scanf("%s",clave);
		    
			if (strcmp(clave,"gato")==0){
				printf("				Intento: %d/3 \n",intento);
                printf("\n  +-------------------------------------------------------------+	");
                printf("\n  |							 /\\_/\\ 	|	");
                printf("\n  |	Enhorabuena, Haz adivinado la palabra secreta	(=^_^=)	|	");
                printf("\n  |							(_____)	|	");
                printf("\n  +-------------------------------------------------------------+ 	\n\n");
                break;
            }
	            else if (strcmp(clave,"Gato")==0){
					printf("				Intento: %d/3 \n",intento);
	                printf("\n  +-------------------------------------------------------------+	");
	                printf("\n  |							 /\\_/\\ 	|	");
	                printf("\n  |	Enhorabuena, Haz adivinado la palabra secreta	(=^_^=)	|	");
	                printf("\n  |							(_____)	|	");
	                printf("\n  +-------------------------------------------------------------+ 	\n\n");
	                break;
	            }

	        else if (i<2){
	            printf("(Palabra incorrecta, intentelo nuevamente)\n");
	            printf("				Intento: %d/3 \n",intento);
	        }
		        else {
		        	printf("				Intento: %d/3 \n",intento);
				}
	        
            intento = intento + 1;
			i++;
			
        } while (i<3);

        if (i==3){
            printf("\n            +--------------------------------------------+   ");
            printf("\n            | Demasiados intentos pa tu casa master xd   |   ");
            printf("\n            +--------------------------------------------+   \n\n ");
        }

    return 0;
}
